package com.example.simetokic.myapplication;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

/**
 * Created by simetokic on 23/02/15.
 */
public class CustomAdapter extends BaseAdapter {

    private LayoutInflater mInflater;
    private Context mContext;

    public CustomAdapter(Context context)
    {
        mContext = context;
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return 10;
    }
    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if(convertView==null)
            convertView = mInflater.inflate(R.layout.list_item_row, parent, false);

        final TextView textOfRow = (TextView) convertView.findViewById(R.id.text);

        textOfRow.setText("This is row number " + position);

        return convertView;

    }
}
