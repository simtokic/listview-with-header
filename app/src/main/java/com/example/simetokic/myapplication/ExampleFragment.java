package com.example.simetokic.myapplication;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;

/**
 * Created by simetokic on 23/02/15.
 */
public class ExampleFragment extends Fragment {

    private ListView mList;
    private Button mButton;
    private Context mContext;


    public ExampleFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main, container, false);

        mList = (ListView)rootView.findViewById(R.id.listView);
        mContext = getActivity().getApplicationContext();

        //header view
        View headerView = inflater.inflate(R.layout.fragment_header_view, null);

        mButton = (Button)headerView.findViewById(R.id.button);

        //add header view to list
        mList.addHeaderView(headerView);

        //set empty adapter, for the head view to display (it must be inflated)
        mList.setAdapter(new BaseAdapter() {
            @Override
            public int getCount() {
                return 0;
            }

            @Override
            public Object getItem(int position) {
                return null;
            }

            @Override
            public long getItemId(int position) {
                return 0;
            }

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                return null;
            }
        });

        mButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mList.setAdapter(new CustomAdapter(mContext));
            }
        });


        return rootView;
    }
}
